This is a Janus Plugged fork.

# Janus Plugged: Vim Distribution

Janus served me well for many years, but it's so damn slow today.

## Copy/Paste easy install (OS X)

```sh
git clone https://github.com/gabrielc63/nvim.git ~/.config/nvim
nvim +PlugInstall +qall
brew install python
brew install python3
pip3 install neovim
pip2 install neovim
```
run :CheckHealth on neovim
